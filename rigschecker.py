#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time, paramiko, re, options
from sqlalchemy import MetaData, Table, or_, create_engine
from sqlalchemy.orm import create_session
from sqlalchemy.ext.declarative import declarative_base
from bs4 import BeautifulSoup
from selenium import webdriver
from socket import error as socket_error

# SQLAlchemy setup
Base = declarative_base()
engine = create_engine(options.rigsdb)
metadata = MetaData(bind=engine)
session = create_session(bind=engine)


class Rig(Base):
    __table__ = Table('rigs', metadata, autoload=True)


# Functions
def log(message):
    print time.strftime("%Y/%m/%d %H:%M:%S") + " ~ " + message


def createObjectFromWorkerId(workerid):
    return session.query(Rig).get(workerid)


def getWorkerIdsOfBadRigs():
    badworkerids = []
    for rig in session.query(Rig).filter(or_(Rig.down == 1, Rig.rebootcount == 1)):
        badworkerids.append(rig.workerid)
    return badworkerids


# Initial setup
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
paramiko.util.log_to_file(".paramiko.log")

url = 'http://ethclassic.ninjapool.jp/#/account/' + options.ninjapoolaccount
query = session.query(Rig.workerid.distinct().label("workerid"))
allworkerids = [row.workerid for row in query.all()]
log('Opening browser...')
browser = webdriver.Firefox()
browser.get(url)
time.sleep(5)

lastlog = ""

while True:
    browser.get(url)
    pagesource = browser.page_source
    workeridsup = []
    rignamesdown = []
    rigsdown = []

    # Scraper
    soup = BeautifulSoup(pagesource, 'html.parser')

    # If the API is down wait, refresh and try again
    if soup(text=re.compile('Stats API Temporarily Down')):
        time.sleep(30)
        browser.refresh()
        time.sleep(5)
        continue

    # If the workers list is in the page go on, otherwise refresh and try again
    if soup(text=re.compile('Your Workers')):
        for tr in soup.findAll('tr', {'class': "success"}):
            workerid = tr('td')[0].contents[0].strip()
            hashrate = tr('td')[options.hashrate_type].contents[0].strip()
            workeridsup.append(workerid)
    else:
        time.sleep(30)
        browser.refresh()
        time.sleep(5)
        continue

    workeridsdown = list(set(allworkerids) - set(workeridsup))

    # Rigs back up
    session.begin()
    for workerid in getWorkerIdsOfBadRigs():
        if workerid in workeridsup:
            rig = createObjectFromWorkerId(workerid)
            rig.down = 0
            rig.rebootcount = 0
            log(rig.name + ' is back up.')
    session.commit()

    # Rigs down
    currentlog = ""
    if workeridsdown:
        for workerid in workeridsdown:
            rig = session.query(Rig).get(workerid)
            rigsdown.append(rig)
        currentlog = "Rigs down: "
        for rig in rigsdown:
            if rig.down == 1:
                currentlog += "\033[1;31m" + rig.name + "\033[0m "
            else:
                currentlog += rig.name + " "
    else:
        currentlog = "No rigs down."
    if currentlog != lastlog:
        log(currentlog)
        lastlog = currentlog

    # Reboot
    # If there are rigs down
    if rigsdown:
        # For every rig
        for rig in rigsdown:
            # If the rig isn't marked as down
            if rig.down == 0:
                # If the rig was rebooted more than 5 minutes ago
                if int(time.time()) - rig.lastreboottime > 300:
                    # If the rig was rebooted more than one time
                    if rig.rebootcount >= 2:
                        # It's down
                        session.begin()
                        rig.down = 1
                        session.commit()
                        log(rig.name + ' marked as down.')
                    else:
                        # Try to reboot
                        try:
                            log('Rebooting ' + rig.name + '...')
                            ssh.connect(rig.ip, username=options.sshuser, password=options.sshpass)
                            stdin, stdout, stderr = ssh.exec_command('/usr/bin/sudo /sbin/reboot -f > /dev/null 2>&1 &')
                            for line in stdout:
                                print line.strip('\n')
                            ssh.close()
                        # If not it's down
                        except (socket_error, paramiko.SSHException):
                            log('Failed to reboot.')
                            log(rig.name + ' marked as down.')
                            session.begin()
                            rig.down = 1
                            session.commit()
                        # If it reboots set stuff
                        else:
                            session.begin()
                            rig.lastreboottime = int(time.time())
                            rig.rebootcount += 1
                            session.commit()

    time.sleep(5.0 - time.time() % 5.0)
