CREATE TABLE "rigs" (
    "workerid" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "mac" TEXT,
    "ip" TEXT NOT NULL,
    "down" INTEGER NOT NULL DEFAULT (0),
    "cardsmissing" INTEGER NOT NULL DEFAULT (0),
    "rebootcount" INTEGER NOT NULL DEFAULT (0),
    "lastreboottime" INTEGER NOT NULL DEFAULT (0)
);
;
